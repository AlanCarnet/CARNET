import java.util.Scanner;


public class Mission4 {


public static void main(String[] args) {
	// TODO Auto-generated method stub

	
	// Initialisation
	String categorie;
	double prix=50;
	// ent age; 	
	//String spectacle1;
	//String spectacle2;    (utilisateur)
	//String spectacle3;
	//String spectacle4;
	
	
	// Traitement
	Scanner sc=new Scanner(System.in);
	
	//Choix du spectacle implique le prix
	System.out.println("Saisissez le numero du spectacle (1,2,3,4) :");
	int spectacle=sc.nextInt();
	
	if (spectacle==1){
		prix=15;
		System.out.println("Le spectacle est : "+spectacle+" avec un prix de : "+prix+ "euros");
	}
	else if (spectacle==2){
		prix=20;
		System.out.println("Le spectacle est : "+spectacle+" avec un prix de : "+prix+ "euros");
	}
	else if (spectacle==3){
		prix=25;
		System.out.println("Le spectacle est : "+spectacle+" avec un prix de : "+prix+ "euros");
	}
	else{
		prix=55;
		System.out.println("Le spectacle est : "+spectacle+" avec un prix de : "+prix+ "euros");
	}
	
	System.out.println("Saisissez l'age du client :");
	int age=sc.nextInt(); //Affectation
	System.out.println("L'age du client est :"+age+" ans");
	
	//Comparaison Age
	if (age<3){
		System.out.println("Vous etes dans la categorie Nourisson");
		categorie="nourisson";
	}			
	else if (3<=age && age<12){
		System.out.println("Vous etes dans la categorie Enfant");
		categorie="enfant";
	}
	else if (12<=age && age<18){
		System.out.println("Vous etes dans la categorie Adolescent");
		categorie="adolescent";
	}
	else if (18<=age && age<=55){
		System.out.println("Vous etes dans la categorie Adulte");
		categorie="adulte";
	}
	else{
		System.out.println("Vous etes dans la categorie Senior");
		categorie="senior";
	}
	
	

	//Categories (avec if)
	if (categorie.equals("nourisson")){
		prix=5;
		System.out.println("En tant que " +categorie+ " age de "+age+" ans, le prix de votre billet sera de "+prix+"euros");
	}
	else if (categorie.equals("enfant")){
		prix=(prix*(0.3));
		System.out.println("En tant que " +categorie+ " age de "+age+" ans, le prix de votre billet sera de "+prix+"euros");
	}	
	else if (categorie.equals("senior")){
		prix=(prix*(0.5));
		System.out.println("En tant que " +categorie+ " age de "+age+" ans, le prix de votre billet sera de "+prix+"euros");
	}
	else{
		System.out.println("En tant que client (adolescent ou adulte) age de "+age+" ans, le prix de votre billet sera de "+prix+"euros");
	}

	
	
	
	
	
	
	}

}




/*Algo Billetrie
Variables : age (entier), categorie (chaine), prix (reel), 	

Debut : 
prix=50
Saisir spectacle
	Si spectacle=1
		prix=15
		Afficher ("Le spectacle est : "+spectacle+" avec un prix de : "+prix+ "euros")
	Sinon spectacle=2
		prix=20
		Afficher ("Le spectacle est : "+spectacle+" avec un prix de : "+prix+ "euros")
	Sinon spectacle=3
		prix=25
		Afficher ("Le spectacle est : "+spectacle+" avec un prix de : "+prix+ "euros")
	Sinon
		prix=55
		Afficher ("Le spectacle est : "+spectacle+" avec un prix de : "+prix+ "euros")
	FinSi
		Saisir age
			Si age<3
			  Alors afficher "Vous etes un nourisson"
			  categorie sto nourisson
			Sinon si 3<=age<12
			  Alors afficher "Vous etes un enfant"
			  categorie sto enfant
			Sinon si 12<=age<18
			  Alors afficher "Vous etes un ado"
			  categorie sto ado
			Sinon si 18<=age<=55
			  Alors afficher "Vous etes un adulte"
			  categorie sto adulte
			Sinon 
			  Afficher "Vous etes un senior"
			  categorie sto senior
	FinSi
	
	Si
		categorie=nourisson
		prix=5
		Afficher ("En tant que " +categorie+ " age de "+age+" ans, le prix de votre billet sera de "+prix+"euros")
	Sinon
		categorie=enfant
		prix=(prix*0.3)
		Afficher ("En tant que " +categorie+ " age de "+age+" ans, le prix de votre billet sera de "+prix+"euros")
	Sinon
		categorie=senior
		prix=prix*0.5
		Afficher ("En tant que " +categorie+ " age de "+age+" ans, le prix de votre billet sera de "+prix+"euros")
	Sinon
		prix=50
		Afficher ("En tant que " +categorie+ " age de "+age+" ans, le prix de votre billet sera de "+prix+"euros")
	FinSi
		
		
Fin			    
*/

