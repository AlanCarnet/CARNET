/*Algo Billetrie
Variables : age (entier), categorie (chaîne), prix (réel), 	

Début : Saisir age
			Si age<3
			  Alors afficher "Vous êtes un nourisson"
			Sinon si 3<=age<12
			  Alors afficher "Vous êtes un enfant"
			Sinon si 12<=age<18
			  Alors afficher "Vous êtes un ado"
			Sinon si 18<=age<=55
			  Alors afficher "Vous êtes un adulte"
			Sinon 
			  Afficher "Vous êtes un sénior"
			FinSi
Fin			    
*/



import java.util.Scanner;


public class Mission4 {


public static void main(String[] args) {
	// TODO Auto-generated method stub

	
	// Initialisation
	String categorie;
	double prix=50;
	// ent age; 	
	//String spectacle1;
	//String spectacle2;    (utilisateur)
	//String spectacle3;
	//String spectacle4;
	
	
	// Traitement
	Scanner sc=new Scanner(System.in);
	
	//Choix du spectacle implique le prix
	System.out.println("Saisissez le numéro du spectacle (1,2,3,4) :");
	int spectacle=sc.nextInt();
	
	if (spectacle==1){
		prix=15;
		System.out.println("Le spectacle est : "+spectacle+" avec un prix de : "+prix+ "€");
	}
	else if (spectacle==2){
		prix=20;
		System.out.println("Le spectacle est : "+spectacle+" avec un prix de : "+prix+ "€");
	}
	else if (spectacle==3){
		prix=25;
		System.out.println("Le spectacle est : "+spectacle+" avec un prix de : "+prix+ "€");
	}
	else{
		prix=55;
		System.out.println("Le spectacle est : "+spectacle+" avec un prix de : "+prix+ "€");
	}
	
	System.out.println("Saisissez l'âge du client :");
	int age=sc.nextInt(); //Affectation
	System.out.println("L'âge du client est :"+age+" ans");
	//Comparaison Age
	if (age<3){
		System.out.println("Vous êtes dans la catégorie Nourisson");
		categorie="nourisson";
	}			
	else if (3<=age && age<12){
		System.out.println("Vous êtes dans la catégorie Enfant");
		categorie="enfant";
	}
	else if (12<=age && age<18){
		System.out.println("Vous êtes dans la catégorie Adolescent");
		categorie="adolescent";
	}
	else if (18<=age && age<=55){
		System.out.println("Vous êtes dans la catégorie Adulte");
		categorie="adulte";
	}
	else{
		System.out.println("Vous êtes dans la catégorie Sénior");
		categorie="senior";
		}
	
	
	//Catégories (avec if)
	if (categorie.equals("nourisson")){
		prix=5;
		System.out.println("En tant que " +categorie+ " agé de "+age+" ans, le prix de votre billet sera de "+prix+"€");
	}
	else if (categorie.equals("enfant")){
		prix=(prix*(0.3));
		System.out.println("En tant que " +categorie+ " agé de "+age+" ans, le prix de votre billet sera de "+prix+"€");
	}	
	else if (categorie.equals("senior")){
		prix=(prix*(0.5));
		System.out.println("En tant que " +categorie+ " agé de "+age+" ans, le prix de votre billet sera de "+prix+"€");
	}
	else{
		System.out.println("En tant que client (adolescent ou adulte) agé de "+age+" ans, le prix de votre billet sera de "+prix+"€");
	}
	
	
	
	
	
	
	
	
	
	}

}
