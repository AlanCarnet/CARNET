def ex() :
    print("Ce programme affiche -1, si le premier nombre entré est plus petit que le second.")
    a = int(input("Entrez un nombre : "))
    b = int(input("Entrez un nombre : "))
    if a<b :
        return -1
    elif a==b :
        return 0
    else :
        return 1
    
def ex1(n) : 
    print("Ce programme calcule le prix de n photocopies :")
    print("20 C les 10 premières,")
    print("15 C les 20 suivantes, et 10 C le reste")
    prix = 0
    if n <= 10 :
        prix = n*20
    elif n > 10 and n <=30 :
        prix = (10*20) + (n-10)*15
    elif n > 30 :
        prix = (10*20) + (n-10)*15 + (n-20)*10
    print("Le prix de : ",n, "photocopies est de : ",prix, "centimes.")
        
from math import *
def ex2() :
    print("Ce programme calcule si l'équation ax²+bx+c = 0 a des solutions.")
    a = int(input("a : "))
    b = int(input("b : "))
    c = int(input("c : "))
    x = int(input("x : "))
    eq = a*(sqrt(x)) + b*x + c

    if eq == 0:
        print(a,b,c,x)
    elif eq != 0 :
        print("pas de soultions")
        
    
def ex3() :
    print("Ce programme affiche tous les nombres depuis celui entré jusqu'à 0.")
    n = int(input("Entrez un nombre positif : "))
    print(n)
    while n != 0 :
        
        print(n-1)
        n = n-1
