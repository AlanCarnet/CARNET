<?php 
$chaine = "je joue du piano";
echo $chaine;
echo "<br />";
if (preg_match("[piano]",$chaine)) //Test pour voir si "piano" est dans la chaine
{
	echo "Oui"; //Oui
} else {
	echo "Non"; //Non
} 
echo "<br />";
 
$search = array('piano'); //tableau de recherche de caractère
$replace = array('PIANO'); //tableau de remplacement de caractère
echo str_replace($search, $replace, $chaine);
echo "<br />";

$chaine2 = "J'adore la programmation";
echo $chaine2;
echo "<br />";
if (preg_match( "[a|e|i|o|u|y|A|E|I|O|U|Y]",$chaine2[0])){
	echo "C'est une voyelle";
}else{
	echo "C'est une consonne";
}
echo "<br />";
if (preg_match( "[programmation$]", $chaine2)){
	echo "Correct ; il s'agit bien du mot programmation";
}else{
	echo "Erreur";
}
echo "<br />";
$chaine3 = "smiley 🙂";
echo $chaine3;
echo "<br />";
if (preg_match( "[🙂]", $chaine3)){
	echo "Oui";
}else{
	echo "Non";
}
$email = "Test@sio.fr";
echo "<br />";
echo $email;
if (preg_match( "[sio.fr$]", $email)){
	echo "<br />";
	echo "Oui";
}else{
	echo "<br />";
	echo "Non";
}
$chaine4 = "Je m'appelle Jade";
echo "<br />";
$chaine4 = str_replace("Jade", "Alan", $chaine4);
echo $chaine4;
?>