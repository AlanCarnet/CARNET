package eu.hautil.dao;

import eu.hautil.modele.Professeur;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.PreparedStatement;
import java.sql.*;
public class ProfesseurDAO {

    public Connection getConnexion(){
        Connection connnection = null;
        String driver = "org.mariadb.jdbc.Driver";
        String url = "jdbc:mariadb://sio-hautil.eu:3306/carnea?user=carnea&password=SPIRITofHEAVEN";
        String user="carnea";
        String mdp = "carnea";
        try{
            Class.forName(driver);
            System.out.println("driver ok");
            connection=ectionDriverManager.getConnection(url,user,mdp);
            System.out.println("connection ok");

        }catch(Exception e){
            e.printStackTrace();
        }
        return conn;
    }

    public void insertProfesseur(Professeur p) throws SQLException {
        String req1 = "INSERT INTO bts_professeurs VALUES (?,?,?)";
        PreparedStatement pstmt = getConnexion().prepareStatement(req1);

        pstmt.setInt(1,p.getId());		//Connexion, puis insertion, puis complète et éxécution.
        pstmt.setString(2,p.getNom());
        pstmt.setString(3,p.getSpecialite());
        int res2= pstmt.executeUpdate();
        System.out.println("nb de modif réalisées : " + res2);
		
    }
    public void deleteProfesseur(Professeur p) throws SQLException {		//Connexion, puis supression, puis complète et éxécution.
        String req2 = "DELETE FROM bts_professeurs WHERE id = ?";
        PreparedStatement supr = getConnexion().prepareStatement(req2);
        supr.setInt(1,p.getId());
        int res3= supr.executeUpdate();
        System.out.println("supr : " + res3);

    }
    public Professeur getProfesseurById(int id) throws SQLException {		//Connexion, puis séléction, puis complète et éxécution.
        Professeur a = null;
        String req6 = "SELECT * FROM bts_professeurs WHERE id = ?";
        PreparedStatement rechNom = getConnexion().prepareStatement(req6);
        rechNom.setInt(1,id);
        ResultSet resId = rechNom.executeQuery();
        if(resId.next()) {
            a = new Professeur(id, resId.getString(2), resId.getString(3));
        }
        resId.close();
        rechNom.close();

        return a;
    }
    public ArrayList<Professeur> getProfesseursBySpecialite(String s) throws SQLException {		//Connection, puis séléction avec une recherche de données et ajout dans une liste list, puis complète et éxécution.
        ArrayList<Professeur> list = new ArrayList<Professeur>();
        String req7 = "SELECT * FROM bts_professeurs WHERE specialite = ?";
        PreparedStatement rechNom = getConnexion().prepareStatement(req7);
        rechNom.setString(1,s);
        ResultSet resSpe = rechNom.executeQuery();
        while(resSpe.next()){
            list.add(new Professeur(resSpe.getInt(1),resSpe.getString(2),resSpe.getString(3)));
        }
        resSpe.close();
        rechNom.close();

        return list;
    }
}
