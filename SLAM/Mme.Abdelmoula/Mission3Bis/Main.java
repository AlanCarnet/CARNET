package eu.hautil;

import eu.hautil.mission3.AdressePostale;

public class Main {

    public static void main(String[] args) {
        //ETAPE 1
        // Créer une adresse postale (no rue xxx, ville, CP)
        // afficher l'adresse postale à l'aide de la méthode "afficher"
        // afficher les informations de l'adresse postales à l'aide des getters
        // modifier une propriété de l'adresse postale
        // réafficher l'adresse pour constater le changement

        AdressePostale a = new AdressePostale("Adresse", "Ville", "Cp");

        a.afficher();

        String adresse = a.getAdresse();
        String ville = a.getVille();        // Definition du type de variable et recuperer les infos.
        String cp = a.getCP();

        System.out.println("Adresse complète : " + adresse + " " + ville + " " + cp);

        a.setCP("A2");
        a.setAdresse("50 rue des Arbres");  // Modifier les valeurs de l'adressePostale
        a.setVille("Cergy");

        adresse = a.getAdresse();
        ville = a.getVille();           // Ne pas redefinir les 3 variables car deja mises avant, juste reprendre les nouvelles valeurs.
        cp = a.getCP();


        System.out.println("Adresse complète : " + adresse + " " + ville + " " + cp);    // Afficher les nouvelles valeurs de l'adresse


        //ETAPE 2
        // créer un bagage (numéro, couleur et poids)
        // afficher le bagage à l'aide de la méthode "afficher"
        // afficher les informations du bagage à l'aide des getters
        // modifier une propriété du bagage
        // réafficher le bagage pour constater le changement

        Bagage valise = new bagage(1, "blanc", 20);

        valise.afficher();

        numero = valise.getNumero();
        couleur = valise.getCouleur();
        poids = valise.Poids();

        valise.setNumero(2);

        System.out.println("Bagage complet : " + numero + " " + couleur + " " + poids);


        //ETAPE 3
        // créer un voyageur (nom, age)
        // afficher le voyageur à l'aide de la méthode "afficher" (on constate qu'il manque une adresse !)
        // ajouter une adresse à ce voyageur (celle créée à l'étape 1)
        // réafficher le voyageur à l'aide la méthode "afficher"
        // afficher les propriétés du voyageur à l'aide des getters
        // modifier le nom de ce voyageur
        // modifier la ville de ce voyageur
        // réafficher le voyageur à l'aide la méthode "afficher" pour constater le changement

        Voyageur v = new Voyageur("TED", 4);
        v.afficher();

        v.setAdresse(a);

        v.afficher();

        nom = v.getNom();
        age = v.getAge();

        adresse = v.getAdresse();
        ville = v.getVille();
        cp = v.getCP();

        v.setNom("TEDX");

        v.setVille("Courdimanche");

        v.afficher();


        //ETAPE 4
        // Compléter le voyageur de l'étape 3
        // ajouter un bagage à ce voyageur (celui créé à l'étape2)
        // afficher le voyageur à l'aide la méthode "afficher"
        // afficher les propriétés du voyageur à l'aide des getters
        // modifier l'âge du voyageur
        // modifier la couleur du bagage du voyageur
        // réafficher le voyageur à l'aide la méthode "afficher" pour constater le changement

        v.setBagage(valise);

        v.afficher();

        nom = v.getNom();
        age = v.getAge();

        adresse = v.getAdresse();
        ville = v.getVille();
        cp = v.getCP();

        numero = v.getNumero();
        couleur = v.getCouleur();
        poids = v.Poids();

        v.setAge(15);

        v.getBagage.setCouleur("Gris");

        v.afficher();


        //ETAPE 5
        // Créer une liste dynamique de 5 voyageurs (nom, age, adresse)
        // afficher tous les voyageurs de la liste à l'aide de la méthode "afficher"


        ArrayList<Voyageur> voyageurs = new ArrayList<>();    // Création liste vide

        Voyageur v1 = new Voyageur("A", 10);
        AdressePostale a1 = new AdressePostale();
        v1.setAdresse(a1);
        voyageurs.add(v1);
        Voyageur v2 = new Voyageur("B", 11);
        AdressePostale a2 = new AdressePostale();
        v2.setAdresse(a2);
        voyageurs.add(v2);
        Voyageur v3 = new Voyageur("C", 12);
        AdressePostale a3 = new AdressePostale();
        v3.setAdresse(a3);
        voyageurs.add(v3);
        Voyageur v4 = new Voyageur("D", 13);
        AdressePostale a4 = new AdressePostale();
        v4.setAdresse(a4);
        voyageurs.add(v4);
        Voyageur v5 = new Voyageur("E", 14);
        AdressePostale a5 = new AdressePostale();
        v5.setAdresse(a5);
        voyageurs.add(v5);

        for (int i = 0; i < voyageurs.size(); i++) { // Parcours les éléments de la liste et les affiches avec les getters.
            Voyageur voyageur = voyageurs.get(i);
            voyageur.afficher();
        }


        //ETAPE 6 (Recherche)
        // Demander à l'utilisateur de saisir un nom de voyageur
        // Chercher ce voyageur dans la liste de l'étape5
        // afficher le voyageur trouvé ou un message de regret


        System.out.println("Saisissez un voyageur");
        String nv = sc.nextLine();
        for (int i = 0; i < voyageurs.size(); i++) {
            if (nv.equals(voyageurs.get(i).getNom())) {
                voyageurs.get(i).afficher();
                verifrech = 1; // Flag (methode), sépare for et if (aidé)
            }
        }
        if (verifrech == 0) { // Si flag est faux affiche :
            System.out.println("Erreur ! La personne renseignée n'existe pas");


            //ETAPE 7 (recherche + suppression)
            // Demander à l'utilisateur de saisir un nom de voyageur
            // Chercher ce voyageur dans la liste de l'étape5
            // supprimer le voyageur trouvé et afficher un message de confirmation  ou afficher un message de regret
            // réafficher la liste des voyageurs


            System.out.println("Saisissez un nouveau voyageur");
            String nv2 = sc.nextLine();
            for (int k = 0; k < voyageurs.size(); k++) {
                if (nv2.equals(voyageurs.get(k).getNom())) {
                    voyageurs.get(k).afficher();
                    verifrech2 = 1;


                    System.out.println("Supprimer le voyageur trouvé ? 0 pour oui et 1 pour non");
                    int supprimer = sc.nextInt();
                    while (supprimer == 0 || supprimer == 1) {
                        if (supprimer == 0) {
                            voyageurs.remove(k);
                            System.out.println("Supprimé");
                            for (int k = 0; k < voyageurs.size(); k++) {
                                Voyageur voyageur = voyageurs.get(i);
                                voyageur.afficher();
                                supprimer = 0;
                            }                                                                       // Erreur avec la variable k
                        } else if (supprimer == 1) {
                            System.out.println("Pas supprimé");
                            for (int k = 0; k < voyageurs.size(); k++) {
                                Voyageur voyageur = voyageurs.get(k);
                                voyageur.afficher();
                                supprimer = 0;
                            }
                        }

                    }
                    if (verifrech2 == 0) {
                        System.out.println("Mauvais nom");
                    }

                }
            }
        }
    }
}