package fr.carnetta;

import java.awt.List;
import java.util.ArrayList;
import java.util.Scanner;


public class Agence {

    private String nom;
    private AdressePostale adresse;
    private ArrayList<Voyageur> VoyageurListe;
    private Bagage bagage;
    Scanner sc = new Scanner (System.in);

    public Agence () {
        this.nom = nom;
        this.adresse = adresse;
        this.VoyageurListe = new ArrayList<Voyageur>();
        Voyageur aa = new Voyageur("A",1);
        Voyageur bb = new Voyageur("B",2);
        Voyageur cc = new Voyageur("C",3);
        Voyageur dd = new Voyageur("D",4);
        VoyageurListe.add(new Voyageur("E",5));
        VoyageurListe.add(aa);
        VoyageurListe.add(bb);
        VoyageurListe.add(cc);
        VoyageurListe.add(dd);
    }


    public Agence(String nom, AdressePostale adresse, ArrayList<Voyageur> VoyageurListe) {
        super();
        this.nom = nom;
        this.adresse = adresse;
        this.VoyageurListe = VoyageurListe;
    }



    public String getNom() {
        return nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }


    public AdressePostale getAdressePostale() {
        return adresse;
    }
    public void setAdressePostale(AdressePostale adresse) {
        this.adresse = adresse;
    }


    public void ajouter(Voyageur x) {
        VoyageurListe.add(x);
    }


    public Bagage getBagage() {
        return bagage;
    }
    public void setBagage(Bagage bagage) {
        this.bagage = bagage;
    }


    public void delete(String nom) {
        for(Voyageur y : VoyageurListe)  {
            if(nom.equals(y.getNom())) {
                System.out.println("Voyageur a supprimer : Si, oui : ecrivez oui, sinon ecrivez non");
                String test = sc.next();
                if (test.equals("oui")) {
                    VoyageurListe.remove(y);
                }
            }
        }
    }
    public void add(String nom) {
        for(Voyageur y : VoyageurListe)  {
            if(nom.equals(y.getNom())) {
                VoyageurListe.add(y);
            }
        }

    }
    public void search(String nom) {
        String voynom = sc.next();
        for(Voyageur x : VoyageurListe)  {
            if(voynom.equals(x.getNom())) {
                x.afficher();
            }

        }
    }




    @Override
    public String toString() {
        return "AgenceVoyage [nom=" + nom + ", adresse=" + adresse + ", VoyageurListe=" + VoyageurListe + "]";
    }







}


