package fr.carnetta;
import java.util.Scanner;
public class Voyageur {
    private int pose;
    private AdressePostale adresse;
    private String ville;
    private String codepostal;
    private Bagage bagage;
    private String nom;
    private int age;

    private String categorie;
    private String libvoie;


    public void setNom(String nom) {
        if (nom.length() >= 2) {
            this.nom = nom;
        }
    }

    public String getNom() {
        return this.nom;
    }


    public void setAge(int age) {
        if (age > 0) {
            this.age = age;
        }
    }

    public int getAge() {
        return this.age;
    }



    public Voyageur() {
        nom = "X";
        age = 0;
        this.categorie = "Bebe";
    }

    public Voyageur(String nom, int age) {
        if (age>0)
            this.age = age;
        else
            this.age = 0;
        if (nom.length() >= 2)
            this.nom=nom;
        else
            this.nom="error";

        this.bagage=bagage;

        this.adresse=adresse;

        if (age < 3) {
            this.categorie = "Bebe";
        } else if (age >= 3 && age < 10) {
            this.categorie = "Enfant";
        } else if (age >= 10 && age < 18) {
            this.categorie = "Adolescent";
        } else if (age >= 18 && age < 55) {
            this.categorie = "Adulte";
        } else {
            this.categorie = "Senior";
        }
    }


    public void setAdresse(AdressePostale adresse) {
        this.adresse = adresse;
    }

    public AdressePostale getAdresse() {return adresse;}

    public void setBagage(Bagage bagage) {
        this.bagage = bagage;
    }

    public Bagage getBagage() {
        return bagage;
    }

    public String getCategorie() { return this.categorie; }

    public void setCategorie(String categorie) {this.categorie = categorie;}

    public int getPose() { return pose; }

    public void setPose(int pose) {this.pose = pose;}

    public String getLibvoie() {return libvoie;}

    public void setLibvoie(String libvoie) {this.libvoie = libvoie;}


    void afficher() {
        System.out.println("Voyageur :(" + nom + "," + age + "," + categorie + ")");
        if (this.adresse != null) {
            this.adresse.afficher();
        } else {
            System.out.println("Pas d'adresse");
        }
        if (this.bagage != null) {
            this.bagage.afficher();
        }
        {
            System.out.println("Pas de bagage");
        }
    }


    public String toString() {
        return "Voyageur [nom=" + nom + ", age=" + age + ", pose=" + pose + ", categorie=" + categorie + ", ville="
                + ville + ", codepostal=" + codepostal + ", libvoie=" + libvoie + ", adresse=" + adresse + ", bagage="
                + bagage + "]";
    }
}
