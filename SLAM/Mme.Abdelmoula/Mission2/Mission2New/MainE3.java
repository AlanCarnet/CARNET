package fr.carnetta;

import java.util.Scanner;
public class MainE3{

    private static Scanner sc;
    public static void main (String arg []) {
        sc = new Scanner(System.in);

        Bagage a = new Bagage(78,"Gris",3);
        a.afficher();

        System.out.println("Renseignez le numero de votre bagage :");
        int numero = sc.nextInt();
        a.setNumero(numero);

        System.out.println("Renseignez ça couleur :");
        String couleur = sc.next();
        a.setCouleur(couleur);

        System.out.println("Renseignez sa masse :");
        float masse = sc.nextFloat();
        a.setMasse(masse);

        a.afficher();
    }
}

