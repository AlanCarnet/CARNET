package fr.carnetta;

public class Bagage {
    private int numero;
    private String couleur;
    private float masse;

    public Bagage(int numero, String couleur, float masse){
        this.numero=numero;
        this.couleur=couleur;
        this.masse=masse;
    }


    public void setNumero(int numero){
        this.numero=numero;
    }
    public int getNumero(){
        return this.numero;
    }


    public void setCouleur(String couleur){
        this.couleur=couleur;
    }
    public String getCouleur(){
        return this.couleur;
    }


    public void setMasse(float masse){
        this.masse=masse;
    }
    public float getMasse(){
        return this.masse;
    }


    void afficher() {
        System.out.println("Bagage : "+numero+" "+couleur+" "+masse+" kg");
    }
}

