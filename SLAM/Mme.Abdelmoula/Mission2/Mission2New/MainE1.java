package fr.carnetta;

import java.util.Scanner;
public class MainE1 {
    private static Scanner sc;
    public static void main (String arg []){
        sc = new Scanner(System.in);

        AdressePostale a = new AdressePostale("X Rue Y","XX", "YY");
        a.afficher();

        System.out.println("Renseignez votre Adresse Postale :");
        String adresse=sc.nextLine();
        a.setAdresse(adresse);

        System.out.println("Renseignez votre Ville :");
        String ville=sc.nextLine();
        a.setVille(ville);

        System.out.println("Renseignez votre Code Postal :");
        String Codepostal=sc.nextLine();
        a.setCodepostal(Codepostal);

        a.afficher();
    }
}

