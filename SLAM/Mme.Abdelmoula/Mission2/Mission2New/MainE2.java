package fr.carnetta;

import java.util.Scanner;
public class MainE2 {

    private static Scanner sc;

    public static void main(String arg[]) {
        sc = new Scanner(System.in);

        AdressePostale b= new AdressePostale("X Rue Y","XX","YY");

        Voyageur a= new Voyageur("Z",1);
        a.setAdresse(b);
        a.afficher();

        System.out.println("Renseignez votre Adresse Postale :");
        String adresse = sc.nextLine();
        b.setAdresse(adresse);

        System.out.println("Renseignez votre Ville :");
        String ville = sc.nextLine();
        b.setVille(ville);

        System.out.println("Renseignez votre Code Postal :");
        String codepostal= sc.nextLine();
        b.setCodepostal(codepostal);

        a.setAdresse(b);
        a.afficher();
    }
}