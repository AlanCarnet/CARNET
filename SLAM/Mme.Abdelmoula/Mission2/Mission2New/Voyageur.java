package fr.carnetta;

public class Voyageur {
    private AdressePostale adresse;
    private Bagage bagage;
    private String categorie;

    private String nom;
    private int age;


    public void setNom(String nom) {
        if (nom.length() >= 2) {
            this.nom = nom;
        }
    }

    public String getNom() {
        return this.nom;
    }


    public void setAge(int age) {
        if (age > 0) {
            this.age = age;
            setCategorie();
        }
    }

    public int getAge() {
        return this.age;
    }


    private void setCategorie() {
        if (age < 3) {
            this.categorie = "Bebe";
        } else if (age >= 3 && age < 10) {
            this.categorie = "Enfant";
        } else if (age >= 10 && age < 18) {
            this.categorie = "Adolescent";
        } else if (age >= 18 && age < 55) {
            this.categorie = "Adulte";
        } else {
            this.categorie = "Senior";
        }
    }

    public String getCategorie() {
        return this.categorie;
    }


    public Voyageur() {
        nom = " ";
        age = 0;
        setCategorie();
    }

    public Voyageur(String nom, int age) {
        this.nom = nom;
        this.age = age;
        setCategorie();
    }


    public void setAdresse(AdressePostale adresse) {
        this.adresse = adresse;
    }

    public AdressePostale getAdresse() {
        return adresse;
    }


    public void setBagage(Bagage bagage) {
        this.bagage = bagage;
    }

    public Bagage getBagage() {
        return bagage;
    }


    void afficher() {
        System.out.println("Voyageur :(" + nom + "," + age + "," + categorie + ")");
        if (this.adresse != null) {
            this.adresse.afficher();
        } else {
            System.out.println("Pas d'adresse");
        }
        if (this.bagage != null) {
            this.bagage.afficher();
        }
        {
            System.out.println("Pas de bagage");
        }
    }
}

