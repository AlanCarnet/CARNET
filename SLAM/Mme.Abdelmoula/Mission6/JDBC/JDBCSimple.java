package fr.carnetta;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class JDBCSimple {
    public static void main(String[] args) {
        try{
            Class.forName("org.mariadb.jdbc.Driver");
            System.out.println("driver ok");
            Connection conn = DriverManager.getConnection("jdbc:mariadb://sio-hautil.eu:3306/carnea?user=carnea&password=SPIRITofHEAVEN");
            System.out.println("connection ok");

            Statement stmt = conn.createStatement();


            /*
            String req1 = "INSERT INTO bts_professeurs VALUES (10,'Abdelmoula','SLAM')";
            int res = stmt.executeUpdate(req1);                                             // Requete a executer 1 fois
            System.out.println("Nb de modif réalisées " + res );
            */


            String req2 = "SELECT nom, specialite FROM bts_professeurs" ;
            int res = stmt.executeUpdate(req2);
            System.out.println("Nb de modifs réalisées " + res);
            ResultSet result = stmt.executeQuery(req2);


          while(result.next()){
              System.out.println(result.getString(1));
              System.out.println(result.getString(2));

          }
            result.close();
            stmt.close();




        }catch(Exception e){
            e.printStackTrace();
        }

    }
}
