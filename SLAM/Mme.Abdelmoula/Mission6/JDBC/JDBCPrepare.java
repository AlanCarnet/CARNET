package fr.carnetta;

import java.util.Scanner;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.PreparedStatement;

public class JDBCPrepare {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);




        try {
            Class.forName("org.mariadb.jdbc.Driver");
            System.out.println("Driver ok");
            Connection conn = DriverManager.getConnection("jdbc:mariadb://sio-hautil.eu:3306/carnea?user=carnea&password=SPIRITofHEAVEN");
            System.out.println("Connection ok");

            System.out.println("Choisissez entre : Entrer un nouveau professeur (1), Supprimer un professeur selon son id (2), Rechercher des professeurs selon leur specialite (3), Rechercher des professeurs selon leur nom (4). ");
            int choix = sc.nextInt();

            if(choix ==1){
                System.out.println("Entrez un id");
                int id1= sc.nextInt();
                System.out.println("Entrez un nom");
                String nom1 = sc.next();
                System.out.println("Entrez une specialite");
                String spe1 = sc.next();
                String req1 = "INSERT INTO bts_professeurs VALUES (?,?,?)";
                PreparedStatement ptstm = conn.prepareStatement(req1);
                ptstm.setInt(1,id1);
                ptstm.setString(2,nom1);
                ptstm.setString(3,spe1);
                int res1 = ptstm.executeUpdate();
            }
            else{
                System.out.println("Erreur dans l'ajout");
            }
            if(choix ==2){
                System.out.println("Entrez l'id du professeur à supprimer");
                int id2 = sc.nextInt();
                String req2 = "DELETE id,nom,specialite FROM bts_professeurs WHERE id = ?";
                PreparedStatement ptstm2 = conn.prepareStatement(req2);
                ptstm2.setInt(1,id2);
                int res2 = ptstm2.executeUpdate();

            }
            else {
                System.out.println("Erreur dans la suppression");
            }
            if (choix ==3){
                System.out.println("Entrez la spécialité recherchée");
                String spe2 =sc.next();
                String req3 = "SELECT FROM bts_professeurs WHERE specialite = ? ";
                PreparedStatement ptstm3 = conn.prepareStatement(req3);
                ptstm3.setString(1,spe2);
                ResultSet res3 = ptstm3.executeQuery();
                while (res3.next()){
                    System.out.println("nom :"+ res3.getString(2));
                    System.out.println("specialite : " + res3.getString(3));

                }

            }
            else{
                System.out.println("erreur dans la recherche de la specialite");
            }
            if (choix ==4){
                System.out.println("Entrez le nom recherché");
                String nom2 =sc.next();
                String req4 = "SELECT id,nom,specialite FROM bts_professeurs WHERE nom = ? ";
                PreparedStatement ptstm4 = conn.prepareStatement(req4);
                ptstm4.setString(1,nom2);
            }
            else{
                System.out.println("erreur dans la recherche du nom");
            }







            System.out.println("Quel nom recherchez-vous?");
            String nom = sc.next();
            String req3 = "SELECT nom FROM bts_professeurs WHERE nom = ?";
            PreparedStatement pstmt2 =conn.prepareStatement(req3);
            pstmt2.setString(1,nom);//1 = place du "?"
            ResultSet result2 = pstmt2.executeQuery();
            while (result2.next()) {
                System.out.println("Nom:"+result2.getString(1));
            }
            result2.close();
            pstmt2.close();
            Statement stmt = conn.createStatement();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
