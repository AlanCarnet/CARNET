public class Etape4 {

	private String nom ; //Initialisations attributs (prive --> setNom)
	private int age ;
	private String categorie ; //Etape3 et 4
	
	
	public void setNom(String nom){ //Methode avec modification du nom. Pour pouvoir utiliser un setNom on fait un void
		if (nom.length()>=2){
			this.nom=nom;
		}
		else
			this.nom= "error";
	}
	
	public void setAge(int age){ //Methode avec modification de l'age
		if (age>0) {		
			this.age = age;

			if (age < 3){		//Etape4 
				categorie = "Nourrisson";
			}

			else if (age >= 3 && age < 12){
				categorie = "Enfant";
			}

			else if (age>=12 && age<18){
				categorie = "Adolescent";
			}

			else if (age>=18 && age<55){
				categorie = "Adulte"; 
			}

			else{
				categorie = "Senior";
			}
		}
		else {
			this.age = 0;
			System.out.println("Erreur");
		}	
	}
	
	
	
	Etape4(String nom, int age)//Methodes, Constructeurs et ajout Etape4
	{
		// this.categorie = categorie ; //Etape3
		this.setAge(age);	//Etape4 pour faire les instructions, sans répétition
		this.setNom(nom);
	}


	Etape4()// Constructeur par défaut
	{
		this.nom = "Alan" ;
		this.age = 19 ;
		this.categorie = "Adulte" ;
	}


	void afficher()// Methode Afficher
	{
		System.out.println(this.nom+ " " + this.age+ " " +this.categorie);
	}
	void affecterValeurs(String string, int i) {
	}
}

