import java.util.Scanner;
public class Main {
	public static void main (String [] args){ // Definition de la page en tant que main
		Scanner sc = new Scanner(System.in);//Initialisations
		System.out.println("Nom ?");
		String nom = sc.nextLine();
		System.out.println("L'âge ?");
		int age = sc.nextInt();
		if (age >0 && nom.length()>=2) { //Etape 2 fonctionnement
			Etape2 premier = new Etape2 (nom, age); //Constructeur caracteristiques valeurs entrees
			Etape2 second = new Etape2 (); //Constructeur par defaut
			premier.setNom(nom);//Modification
			premier.afficher();
			second.afficher() ;
		}
		else if (age<0){
			System.out.println("Erreur sur l'age");
		}

		else if (nom.length()<2){
			System.out.println("Erreur sur la taille du nom");
		}
	}
}