package eu.siohautil.main;

import eu.siohautil.heritage.Vehicule;
import eu.siohautil.heritage.Chien;
import eu.siohautil.heritage.Voiture;
import eu.siohautil.heritage.Bateau;
import eu.siohautil.heritage.Navigable;
import eu.siohautil.heritage.Canard;
import eu.siohautil.heritage.Animal;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    private static Scanner sc;

    public static void main(String[] args) {
        sc = new Scanner(System.in);
        ArrayList<Vehicule> v = new ArrayList<Vehicule>();
        Voiture v1 = new Voiture("Opel", "Tigra", "Jaune",4);
        Bateau v2 = new Bateau("NorthWest", "Compass Tiller","Blanc",187);

        v.add(v1);
        v.add(v2);

        for (Vehicule t : v){
            t.demarrer();
            //t.stationner(); Ne peut pas fonctionner, puisque stationner n'existe pas dans Vehicule
        }

        ArrayList<Animal> a = new ArrayList<Animal>();
        Chien a1 = new Chien("Pilou", "Bichon Maltais", 8,"Blanc");
        Canard a2 = new Canard("Donald", 30,"Duck",8000);

        a.add(a1);
        a.add(a2);

        for (Animal r : a){
            r.afficher();
            //r.aboyer(); Ne peut fonctionner, puisque aboyer n'existe pas dans Canard
        }

        ArrayList<Navigable> n = new ArrayList<Navigable>();
        Bateau n1 = new Bateau("NorthWest", "Compass Outboard", "Blanc", 207);
        Canard n2 = new Canard("Daffy", 35, "Duck", 8500);

        n.add(n1);
        n.add(n2);

        for (Navigable e : n){
            e.naviguer();
            //e.afficher();  Ne peut fonctionner puisque afficher n'existe pas dans Bateau
        }


    }
}