package fr.carnetta;

public class Voiture extends Vehicule {

    private int place;

    public Voiture(String marque, String couleur, String modele,  int place){
        super(marque, couleur, modele);
        this.place = place;
    }

    public void demarrer(){
        System.out.println("Voiture :" + this.getMarque() + " " + this.getModele() + " " + this.getCouleur() + " qui démarre.");
    }

    public void freiner(){
        System.out.println("Voiture :" + this.getMarque() + " " + this.getModele() + " " + this.getCouleur() + " qui freine.");
    }

    public void stationner(){
        System.out.println("Voiture :" + this.getMarque() + " " + this.getModele() + " " + this.getCouleur() + " qui stationne.");
    }
}
