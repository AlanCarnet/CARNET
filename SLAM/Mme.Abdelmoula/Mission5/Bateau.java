package fr.carnetta;

public class Bateau extends Vehicule implements Navigable {
    
    private int volume;

    public Bateau(String marque, String modele, String couleur, int volume) {
        super(marque, modele, couleur);
        this.volume = volume;
    }

    @Override
    public void demarrer() {System.out.println("Bateau " + this.getModele() + "qui demarre.");}

    @Override
    public void freiner() {System.out.println("Bateau " + this.getModele() + "qui freine.");}
	
	public void accoster() {System.out.println("Bateau " + this.getModele() + "qui accoste.");}

    public void naviguer() {System.out.println("Bateau " + this.getModele() + "qui navigue.");}

    public void couler() {System.out.println("Bateau " + this.getModele() + "qui coule.");}

}