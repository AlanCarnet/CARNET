package fr.carnetta;

public class Chien extends Animal{

    private String couleur;

    public Chien(String nom, String type, int age, String couleur) {
        super(nom, age, type);
        this.couleur = couleur;
    }

    public void aboyer(){
        System.out.println("Chien "+ this.getNom() + "qui aboie.");
    }
}
