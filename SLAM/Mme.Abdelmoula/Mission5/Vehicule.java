package fr.carnetta;

public abstract class Vehicule {

    private String marque;

    private String couleur;

    private String modele;

    public Vehicule(String marque, String couleur, String modele) {
        this.marque = marque;
        this.couleur = couleur;
		this.modele = modele;    
    }

	public abstract void demarrer();
    public abstract void freiner();

    public String getMarque(){return marque;}

	public String getCouleur(){return couleur;}
    
	public String getModele() {return modele;}
        
}
