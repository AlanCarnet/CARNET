package fr.carnetta;

public class Animal {

    private String nom;
	private int age;
    private String type;
    
	public Animal(){

    }

    public Animal(String nom, int age, String type) {
        this.nom = nom;
        this.age = age;
        this.type = type;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setType(String type) {
        this.type = type;
    }


    public String getNom(){
        return this.nom;
    }

    public int getAge(){
        return this.age;
    }

    public String getType(){
        return this.type;
    }

    public void afficher(){
        System.out.println("Animal : " + getNom() +  " age " + getAge() + "de ans.");
    }
}