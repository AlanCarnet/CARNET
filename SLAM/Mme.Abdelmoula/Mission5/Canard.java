package fr.carnetta;

public class Canard extends Animal implements Navigable {

    private int plumes;

    public Canard( String nom, int age , String type, int plumes ) {
        super(nom, age, type);
        this.plumes = plumes ;
    }

    public void naviguer() {
        System.out.println ("Canard "+ this.getNom() + "qui navigue.");
    }
	
	public void accoster() {
        System.out.println ("Canard "+ this.getNom() + "qui accoste.");
    }

    public void couler() {
        System.out.println("Canard " + this.getNom() + "qui coule.");
    }
}
